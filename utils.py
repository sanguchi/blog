import re
import unicodedata

from objects import Entry


def generate_slug(title):
    slug = unicodedata.normalize("NFKD", title).encode("ascii", "ignore").decode("ascii")
    slug = re.sub(r"[^\w]+", " ", slug)
    slug = "-".join(slug.lower().strip().split())
    if not slug:
        slug = "entry"
    # print("slug : ", slug)
    while True:
        e = Entry.select().where(Entry.slug == slug)
        
        if(e.count() == 0):
            break
        print("count : ", e.count())
        slug += "-2"
        print("slug : ", slug)
        # sleep(1)
    return slug
