#!/usr/bin/env python

from objects import Author, Entry, Comment, Message
import utils

from peewee import DoesNotExist
import bcrypt
import concurrent.futures
import markdown
import os.path
from tornado import gen
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options

define("port", default=8888, help="run on the given port", type=int)

# A thread pool to be used for password hashing with bcrypt.
executor = concurrent.futures.ThreadPoolExecutor(2)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", HomeHandler),
            (r"/archive", ArchiveHandler),
            # (r"/feed", FeedHandler),
            (r"/entry/([^/]+)", EntryHandler),
            (r"/compose", ComposeHandler),
            (r"/comment/create", CommentHandler),
            (r"/comment/delete", CommentDeleteHandler),
            (r"/auth/create", AuthCreateHandler),
            (r"/auth/login", AuthLoginHandler),
            (r"/auth/logout", AuthLogoutHandler),
            # (r"/messages", MessagesHandler),
        ]
        settings = dict(
            blog_title=u"Tornado Blog",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            ui_modules={
                "Entry": EntryModule,
                "Comment": CommentModule,
                "CommentDeleteButton": CommentDeleteModule,
                "Header": HeaderModule,
                "DropdownMenu": DropMenuHandler},
            xsrf_cookies=True,
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            login_url="/auth/login",
            debug=True,
        )
        super(Application, self).__init__(handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        try:
            aut = Author.get(Author.id == self.get_secure_cookie("blogdemo_user"))
            return aut
        except DoesNotExist:
            return False

    def any_author_exists(self):
        return Author.select().count() > 0


class HomeHandler(BaseHandler):
    def get(self):
        entries = Entry.select().order_by(-Entry.published)[:5]
        if len(entries) == 0:
            self.redirect("/compose")
        else:
            self.render("home.html", entries=entries)


class EntryHandler(BaseHandler):
    def get(self, slug):
        entry = Entry.select().where(Entry.slug == slug)
        if(entry.count() == 0):
            raise tornado.web.HTTPError(404)
        else:
            self.render("entry.html", entry=entry[0])


class ArchiveHandler(BaseHandler):
    def get(self):
        entries = Entry.select().order_by(-Entry.published)
        self.render("archive.html", entries=entries)

'''
class FeedHandler(BaseHandler):
    def get(self):

        entries = self.db.query("SELECT * FROM entries ORDER BY published "
                                "DESC LIMIT 10")
        self.set_header("Content-Type", "application/atom+xml")
        self.render("feed.xml", entries=entries)
'''


class ComposeHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        id = self.get_argument("id", None)
        if id:
            try:
                entry = Entry.get(Entry.id == int(id))
            except DoesNotExist:
                entry = False
        else:
            entry = False
        self.render("compose.html", entry=entry)

    @tornado.web.authenticated
    def post(self):
        id = self.get_argument("id", None)
        title = self.get_argument("title")
        text = self.get_argument("markdown")
        html = markdown.markdown(text)
        if id:
            try:
                entry = Entry.get(Entry.id == int(id))
            except DoesNotExist:
                raise tornado.web.HTTPError(404)
            entry.title = title
            entry.markdown = text
            entry.html = html
            entry.save()
            slug = entry.slug
        else:
            slug = utils.generate_slug(title)
            print('POST slug: ', slug)
            e = Entry.create(slug=slug, author=self.get_current_user(), title=title, markdown=text, html=html)
            e.save()

        self.redirect("/entry/" + slug)


class CommentHandler(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        id = self.get_argument("id", None)
        message = self.get_argument("message", None)
        author = self.get_secure_cookie("blogdemo_user")
        try:
            entry = Entry.get(Entry.id == int(id))
        except DoesNotExist:
            raise tornado.web.HTTPError(404)

        comment = Comment.create(author=author, entry=entry, message=message)
        comment.save()
        self.redirect("/entry/" + entry.slug)


class CommentDeleteHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        id = self.get_argument("id", None)
        cid = self.get_argument("cid", None)
        try:
            entry = Entry.get(Entry.id == int(id))
            comment = Comment.get(Comment.entry == entry, Comment.id == cid)
        except DoesNotExist:
            raise tornado.web.HTTPError(404)
        comment.delete_instance()
        self.redirect("/entry/" + entry.slug)


class AuthCreateHandler(BaseHandler):
    def get(self):
        self.render("create_author.html", error=False)

    @gen.coroutine
    def post(self):
        email=self.get_argument("email")
        username=self.get_argument("username")
        hashed_password = yield executor.submit(
            bcrypt.hashpw, tornado.escape.utf8(self.get_argument("password")),
            bcrypt.gensalt())

        a = Author.select().where(Author.username == self.get_argument("username"))
        if(a.count() == 0):
            a = Author.select().where(Author.email == self.get_argument("email"))
            if (a.count() == 0):
                print('Registered: ', email, username, hashed_password)
                a = Author.create(email=email, username=username, hashed_password=hashed_password)
                a.save()
                self.set_secure_cookie("blogdemo_user", str(a.id))
                self.redirect("/")
            else:
                self.render("create_author.html", error='Email taken')
        else:
            self.render("create_author.html", error='Username taken')


class AuthLoginHandler(BaseHandler):
    def get(self):
        # If there are no authors, redirect to the account creation page.
        if not self.any_author_exists():
            self.redirect("/auth/create")
        else:
            self.render("login.html", error=None)

    @gen.coroutine
    def post(self):
        try:
            author = Author.get(Author.username == self.get_argument("username"))
        except DoesNotExist:
            self.render("login.html", error="Invalid username")
            return
        hashed_password = yield executor.submit(
            bcrypt.hashpw, tornado.escape.utf8(self.get_argument("password")),
            tornado.escape.utf8(author.hashed_password))
        hashed_password = hashed_password.decode('utf-8')
        # print('Hashed:\n%s\nStored:\n%s' %(hashed_password, author.hashed_password))
        if(hashed_password == author.hashed_password):
            self.set_secure_cookie("blogdemo_user", str(author.id))
            self.redirect("/")
        else:
            self.render("login.html", error="incorrect password")


class AuthLogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("blogdemo_user")
        self.redirect("/")


class MessagesHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        user = self.get_current_user()
        sent = Message.select(Message.author == user)# .order_by(-Message.published)
        received = Message.select(Message.to == user)# .order_by(-Message.published)
        total = sent.count() + received.count()
        if(total == 0):
            status = "You don't have messages."
        else:
            status = "%s total Messages." % total
        self.render("messages.html", sent=sent, received=received, error=False, status=status)

    @tornado.web.authenticated
    def post(self):
        error = False
        status = False
        user = self.get_current_user()
        text = self.get_argument("text")
        if(text == ''):
            error = 'Empty message.'
        try:
            to_user = Author.get(Author.username == self.get_argument("to_user"))
        except DoesNotExist:
            error = 'Invalid username.'
        if(not error):
            Message.create(author=user, to=to_user, text=text).save()
            status = 'Message sent.'
        sent = Message.select(Message.author == user).order_by(-Message.published)
        received = Message.select(Message.to == user).order_by(-Message.published)
        self.render("messages.html", sent=sent, received=received, error=error, status=status)


class EntryModule(tornado.web.UIModule):
    def render(self, entry):
        return self.render_string("modules/entry.html", entry=entry)

    def javascript_files(self):
        return ['timeparser.js']

class CommentModule(tornado.web.UIModule):
    def render(self, comment):
        return self.render_string("modules/comment.html", comment=comment)


class CommentDeleteModule(tornado.web.UIModule):
    def render(self, comment):
        if(self.current_user):
            if(self.current_user.username == 'root'
               or self.current_user.username == comment.entry.author.username
               or self.current_user.username == comment.author.username):
                will_render = True
            else:
                will_render = False
        else:
            will_render = False

        return self.render_string("modules/delete-comment.html", comment=comment, render=will_render)


class HeaderModule(tornado.web.UIModule):
    def render(self, author):
        return self.render_string("modules/header.html", author=author)

    def css_files(self):
        return ['header.css']


class DropMenuHandler(tornado.web.UIModule):
    def render(self, author):
        return self.render_string("modules/user-dropdown.html", author=author)


def main():
    tornado.options.parse_command_line()
    print("Loading server on port ", options.port)
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
