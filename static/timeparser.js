/**
 * Created by arch on 04/08/16.
 */

var xpath = function(xpathToExecute){
  var result = [];
  var nodesSnapshot = document.evaluate(xpathToExecute, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null );
  for ( var i=0 ; i < nodesSnapshot.snapshotLength; i++ ){
    result.push( nodesSnapshot.snapshotItem(i) );
  }
  return result;
}
var datespath = '//span[@class="date"]';
var dates = xpath(datespath);
for (var i = 0; i < dates.length; i++)
{
  var d = new Date(parseInt(dates[i].innerHTML) * 1000);
  dates[i].innerHTML = d.toDateString() + " " + d.toLocaleTimeString();
}