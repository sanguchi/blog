#!/bin/bash

export SNAME="blog-session";

echo "CURRENT DIR: $PWD";

if tmux has -t $SNAME; then
  tmux kill-session -t $SNAME;
  echo "$SNAME session was killed."
fi
tmux new-session -dP -s $SNAME;
echo "Session $SNAME created."
tmux send-keys -t $SNAME 'virtualenv venv --python python3' C-m;
tmux send-keys -t $SNAME 'source venv/bin/activate' C-m;
tmux send-keys -t $SNAME 'pip install -r requirements.txt' C-m;
tmux send-keys -t $SNAME 'python blog.py --port=2574' C-m;
echo "Sent keys to $SNAME"

