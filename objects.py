from peewee import *
import datetime
db = SqliteDatabase('sqlite.db')


class BaseModel(Model):
    class Meta:
        database = db


class Author(BaseModel):
    email = CharField(unique=True)
    username = CharField(unique=True)
    hashed_password = CharField()


class Entry(BaseModel):
    slug = TextField(unique=True)
    author = ForeignKeyField(Author, related_name='entries')
    title = TextField()
    markdown = TextField()
    html = TextField()
    published = DateTimeField(default=datetime.datetime.now)


class Comment(BaseModel):
    author = ForeignKeyField(Author, related_name='comments')
    entry = ForeignKeyField(Entry, related_name='comments')
    message = TextField()
    published = DateTimeField(default=datetime.datetime.now)


class Message(BaseModel):
    author = ForeignKeyField(Author, related_name='messages_sent')
    to = ForeignKeyField(Author, related_name='messages_received')
    text = TextField()
    published = DateTimeField(default=datetime.datetime.now)
    read = BooleanField(default=False)


# Only create the tables if they do not exist.
db.create_tables([Author, Comment, Entry, Message], safe=True)
